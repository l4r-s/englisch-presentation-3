#HSLIDE

![LOGO](https://cdn.meme.am/cache/instances/folder270/67272270.jpg)

#### infrastructure as Code
<span class="fragment" data-fragment-index="1"><span style="color:gray">or maybe config management...</span>
<br>
<span class="fragment" data-fragment-index="2"><span style="color:gray">or automate everything...</span>
<br>
<span class="fragment" data-fragment-index="3"><span style="color:gray">not quite sure :)</span>

#HSLIDE
## <b><span style="color: #e49436; text-transform: none">Word List</span></b>
| git                         | Most used version control system         |
|-----------------------------|------------------------------------------|
| opensource                  | the underlying code of a software is public available and can be (in most cases) modified from everybody |
| github                      | Biggest opensource community, uses git for all repositorys |
| cloud                       | a system which can be controlled over an API and provides compute, storage and network ressources |

#HSLIDE

## <b><span style="color: #e49436; text-transform: none">Content</span></b>
<br>
### <span class="fragment" data-fragment-index="1">what is <span style="color: #e49436">Infrastructure as Code</span>?

### <span class="fragment" data-fragment-index="2">about <span style="color: #e49436">tools</span>

### <span class="fragment" data-fragment-index="3">about <span style="color: #e49436">testing</span>

### <span class="fragment" data-fragment-index="4">demo <span style="color: #e49436">time</span>

### <span class="fragment" data-fragment-index="5">my <span style="color: #e49436">conclusion</span>

#HSLIDE

## <span style="color: #e49436">Infrastructure as Code</span>
<br>
### everything in your infrastructure is defined in <span style="color: #e49436">code</span>

### <span class="fragment" data-fragment-index="1">or at least <span style="color: #e49436">some</span> parts of it

### <span class="fragment" data-fragment-index="2">track all your changes in a <span style="color: #e49436">git</span> repository

#HSLIDE

## <span style="color: #e49436">Infrastructure as Code</span>
<br>
### <span class="fragment" data-fragment-index="1"><span style="color: #e49436">automate</span> boring recursive tasks

### <span class="fragment" data-fragment-index="2">get a higher quality with automated <span style="color: #e49436">testing</span>

### <span class="fragment" data-fragment-index="3">save <span style="color: #e49436">money</span>

#HSLIDE

## <span style="color: #e49436">Tools</span>
<br>
### <span class="fragment" data-fragment-index="1">code needs to be <span style="color: #e49436">interpreted</span>

### <span class="fragment" data-fragment-index="2">and <span style="color: #e49436">deployed</span>

#HSLIDE

## <span style="color: #e49436">Tools</span>
<br>
#### <span class="fragment" data-fragment-index="1">terraform | <span style="color: #e49436">terraform.io</span>

#### <span class="fragment" data-fragment-index="2">puppet | <span style="color: #e49436">puppet.com</span>

#### <span class="fragment" data-fragment-index="3">ansible | <span style="color: #e49436">ansible.com</span>

#### <span class="fragment" data-fragment-index="4">chef | <span style="color: #e49436">chef.io</span>

#### <span class="fragment" data-fragment-index="5">docker | <span style="color: #e49436">docker.com</span>

#### <span class="fragment" data-fragment-index="6">vagrant | <span style="color: #e49436">vagrantup.com</span>

#HSLIDE

## <span style="color: #e49436">Tools - terraform</span>
~~~~
resource "digitalocean_droplet" "www1.example.com" {
    image = "ubuntu"
    name = "www1.example.com"
    region = "fra1"
    size = "512mb"
    ssh_keys = [
      "${var.ssh_fingerprint}"
    ]
    provisioner "remote-exec" {
        scripts = ["puppet apply" ]
    }
  }
~~~~

#HSLIDE

## <span style="color: #e49436">Tools - puppet</span>
~~~~
node 'www1.example.com' {
  include common
  include nginx
}
node 'db1.example.com' {
  include common
  include mysql
}
~~~~

#HSLIDE

## <span style="color: #e49436">Tools</span>
<br>
### <span class="fragment" data-fragment-index="1"><span style="color: #e49436">cloud</span> "enabled" infrastructure

### <span class="fragment" data-fragment-index="2"><span style="color: #e49436">public</span> cloud

##### <span class="fragment" data-fragment-index="3">Digitalocean

##### <span class="fragment" data-fragment-index="3">Google Cloud Engine

##### <span class="fragment" data-fragment-index="3">AWS

#HSLIDE

## <span style="color: #e49436">Tools</span>
<br>
## Private <span style="color: #e49436">Cloud </span>
##### <span class="fragment" data-fragment-index="1">OpenStack
##### <span class="fragment" data-fragment-index="2">Kubernetes self hosted

### <span class="fragment" data-fragment-index="3">or at least some <span style="color: #e49436">API</span> support
##### <span class="fragment" data-fragment-index="4">vmWare vSphere
##### <span class="fragment" data-fragment-index="5">Microsoft Hyper-V

#HSLIDE

## <span style="color: #e49436">Testing</span>
<br>
### <span class="fragment" data-fragment-index="1">recognize failures <span style="color: #e49436">faster</span>

### <span class="fragment" data-fragment-index="2">before they reach <span style="color: #e49436">production</span>

### <span class="fragment" data-fragment-index="3"><span style="color: #e49436">avoid</span> making the same mistake twice

#HSLIDE

## <span style="color: #e49436">Testing</span>
<br>
![testing](testing-workflow.png)

#HSLIDE

## <span style="color: #e49436">Demo Time</span>

#HSLIDE

## <span style="color: #e49436">conclusion</span>
<br>
### <span class="fragment" data-fragment-index="1">very high <span style="color: #e49436">reusability</span>

### <span class="fragment" data-fragment-index="2"><span style="color: #e49436">quality</span> guarantee

### <span class="fragment" data-fragment-index="3">track <span style="color: #e49436">changes</span>

### <span class="fragment" data-fragment-index="4"><span style="color: #e49436">opensource</span> community

#HSLIDE

## <span style="color: #e49436">questions?</span>
## <span class="fragment" data-fragment-index="1">Thank you!
